
public class Variaveisinteiras {

	public static void main(String[] args) {
		
		byte idade01 = 29;
		short idade02 = 29;
		int idade03 = 29;
		long idade04 = 29;
		//as variaveis int e long s�o mais usadas devido sua quantidade de intervalo entre os valores
		//byte representa o intervalo de valores de -128 a 127 
		//short representa o intervalo de valores de -32.768 a 32.767
		//int representa o intervalo de valores de -2.147.483.648 a 2.147.483.647
		//long representa o intervalo de valores de -9.223.372.036.854.775.808 a 9.223.372.036.854.775.807

		System.out.println("Idade em byte = " + idade01);
		System.out.println("Idade em short = " + idade02);
		System.out.println("Idade em int = " + idade03);
		System.out.println("Idade em long = " + idade04);
		
		
		
		double valorDaCerveja = 4.50;
		float valorDaCerveja2 = 4.50f;
		//double tem um tamanho de ate 64bits
		//float tem um tamanho de ate 32bits
		//tem que usar a letra F no float porque o double � comumente mais usado.
		
		System.out.println("Valor da cerveja em double = " + valorDaCerveja);
		System.out.println("Valor da cerveja em float = " + valorDaCerveja2);
		

		char H = 'H';
		char u = 'u';
		char g = 'g';
		char o = 'o';
		char exclamacao = '!';
		//os valores das variaveis char podem ser representadas pelos numeros correspodente na tabela ASCII
		//char H = 72;
		//char u = 117;
		//char g = 103;
		//char o = 111;
		//char exclamacao = 33;
		
		System.out.println(""+H+u+g+o+exclamacao);
		
		
		
		boolean verdadeiro = true;
		boolean falso = false;
		
		System.out.println("O valor de verdadeiro � " + verdadeiro);
		System.out.println("O valor de falso � " + falso);

	}

}
